document.addEventListener("DOMContentLoaded", event => {

    // Récupérer la liste des zones
    let liste_zone = document.querySelectorAll(".zone");
    
    liste_zone.forEach( zone => {

        // Définir les coordonées (absolut) du flag
        let x = zone.getBoundingClientRect().left + (zone.getBoundingClientRect().width / 2) - 16;
        let y = zone.getBoundingClientRect().top + (zone.getBoundingClientRect().height / 2) - 32;

        // Créer le flag (balise image)
        let flag = document.createElement("img");
        flag.src = "static/img/maps-and-flags.svg";
        flag.classList.add("flag");
        flag.style.left = x + "px";
        flag.style.top = y + "px";
        document.body.appendChild(flag);
    });
});