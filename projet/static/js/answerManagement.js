function answer(idRadioGroup, idCorrectAnswer, idResultZone, message="")
{
    let res = document.querySelector(idResultZone);
    res.innerHTML = "";
    let value = document.createTextNode("");
    let div = document.createElement("div");
    div.setAttribute("role", "alert");
    div.classList.add("alert");
    justification = document.createTextNode("\n"+message); 

    if (document.querySelector(idRadioGroup+" "+idCorrectAnswer).checked == true)
    {
        div.classList.add("alert-success");
        value = document.createTextNode("Bonne reponse !");
        div.appendChild(value);
        div.appendChild(justification);
    }   
    else
    {
        div.classList.add("alert-danger")
        value = document.createTextNode("Mauvaise reponse !");
        div.appendChild(value);
    }
    
    res.appendChild(div);
}